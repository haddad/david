## DAVID, a real-time emotional voice transformation tool ##
>  “Like an auto-tune, but for emotions” (Brian Resnick, for [Vox.com](https://www.vox.com/science-and-health/2016/1/17/10777304/sound-of-your-own-voice-affect-mood))
DAVID was especially designed with the affective psychology and neuroscience community in mind, and aims to provide researchers with new ways to produce and control affective stimuli, both for offline listening and for real-time paradigms. For instance, we have used it to create real-time emotional vocal feedback in [Aucouturier et al, 2016](https://www.vox.com/science-and-health/2016/1/17/10777304/sound-of-your-own-voice-affect-mood).

Technically, DAVID is implemented as an open-source patch for the close-source audio processing plateform [Max](http://forumnet.ircam.fr/shop/fr/partenaires/87-max-7.html) [(Cycling’74)](https://cycling74.com), and, like Max, is available for most Windows and MacOS configurations.

##  What does it do ? ##

DAVID is a software tool able to “add” emotion to a speech recording, i.e. it can make that American chap

<p>
    <audio src="https://forum.ircam.fr/media/uploads/Softwares/DAVID/06-m4_long.mp3" controls>
        https://forum.ircam.fr/media/uploads/Softwares/DAVID/06-m4_long.mp3
    </audio>
</p>
or that French young lady

<p>
    <audio src="https://forum.ircam.fr/media/uploads/Softwares/DAVID/09-f1_long.mp3" controls>
        https://forum.ircam.fr/media/uploads/Softwares/DAVID/09-f1_long.mp3
    </audio>
</p>
sound anxious

<p>
    <audio src="https://forum.ircam.fr/media/uploads/Softwares/DAVID/08-m4_long_afraid_2.mp3" controls>
        https://forum.ircam.fr/media/uploads/Softwares/DAVID/08-m4_long_afraid_2.mp3
    </audio>
</p>
<p>
    <audio src="https://forum.ircam.fr/media/uploads/Softwares/DAVID/03-f1_long_afraid.mp3" controls>
        https://forum.ircam.fr/media/uploads/Softwares/DAVID/03-f1_long_afraid.mp3
    </audio>
</p>
happy

<p>
    <audio src="https://forum.ircam.fr/media/uploads/Softwares/DAVID/01-m4_long_happy.mp3" controls>
        https://forum.ircam.fr/media/uploads/Softwares/DAVID/01-m4_long_happy.mp3
    </audio>
</p>
<p>
    <audio src="https://forum.ircam.fr/media/uploads/Softwares/DAVID/04-f1_long_happy.mp3" controls>
        https://forum.ircam.fr/media/uploads/Softwares/DAVID/04-f1_long_happy.mp3
    </audio>
</p>
or sad

<p>
    <audio src="https://forum.ircam.fr/media/uploads/Softwares/DAVID/02-m4_long_sad1.mp3" controls>
        https://forum.ircam.fr/media/uploads/Softwares/DAVID/02-m4_long_sad1.mp3
    </audio>
</p>
<p>
    <audio src="https://forum.ircam.fr/media/uploads/Softwares/DAVID/05-f1_long_sad1.mp3" controls>
        https://forum.ircam.fr/media/uploads/Softwares/DAVID/05-f1_long_sad1.mp3
    </audio>
</p>
(and whatever else you’d like them to sound like).
<br>

It does so in a way that’s properly validated for behavioural science, i.e. emotions are recognizable by listeners, they are fully controllable and reproducible in intensity, and there are mistaken as natural expressions and not detected as synthetic [(Rachman et al, 2017)](https://link.springer.com/article/10.3758/s13428-017-0873-y). In fact, even the speakers themselves typically mistake the manipulated speech as their own.

In addition, it does so in real-time, i.e. you can transform speech as it is spoken, e.g. over the phone or onstage. With modern audio interfaces, we reached in/out latencies as small as 15ms, which even makes it useable as vocal feedback to a speaker without disrupting speech production.

## Installation ##
DAVID is implemented as an open-source patch for the close-source audio processing plateform [Max](http://forumnet.ircam.fr/shop/fr/partenaires/87-max-7.html) [(Cycling’74)](https://cycling74.com). To use DAVID, you need to install Max first.

##### 1. First step: Installing Max #####

DAVID has been fully tested with [Max 8](https://forum.ircam.fr/projects/detail/max-8/), and also supports Max 6 and 7. Max is available in MacOS and Windows versions on the [seller’s website](https://cycling74.com/downloads).
Max is available under a commercial license from [(Cycling’74)](https://cycling74.com), but is available in free runtime versions that suffice to run DAVID.

According to its seller Cycling’74, system requirements for Max 8 are Intel® Mac with Mac OS X 10.11.6 or later, Intel® Core™2 Duo processor; Intel® Core™ i5 processor or faster recommended, 4 GB RAM (8 GB or more recommended). Or, a PC with Windows 7, Windows 8 or Windows 10, 64-bit Intel® or AMD multi-core processor. Intel® Core™ i5 processor or faster recommended, and GB RAM (8 GB or more recommended). (Note: For 32-bit systems, use Max 7.3.5 or older.) If your system widely departs from these specifications, consider installing [Max6](https://cycling74.com/downloads/older/).

##### 2. First step: downloading  DAVID #####

DAVID V1.2 was released on January 31st, 2017. As of March 2017, DAVID is available as a free download on the IRCAM Forum, the community for science and art users of audio software developed in the IRCAM community. Download free from this page. Finally, to open the patch with Max, double click on the DAVID.maxproj file.

>- [Max 8](https://www.ircam.fr/innovations/abonnements-et-logiciels//) is available at the IRCAM Forum.

## Tutorial and Usage documentation ##
See the [readme](http://forumnet.ircam.fr/wp-content/uploads/2017/03/README1.txt).

The following [video](https://player.vimeo.com/video/146702945) is a tutorial presented by [Marco Liuni] (https://www.ircam.fr/person/marco-liuni/) (IRCAM/CNRS) at the 4th International Conference on Music and Emotion, October 2015 in Geneva (Switzerland).

In addition, the following extract is a report by [Jean-Julien Aucouturier](https://www.ircam.fr/person/jean-julien-aucouturier/) and [Laura Rachman](https://www.ircam.fr/person/laura-rachman/) (IRCAM/CNRS) on some of the effects’ experimental validation, presented at the same conference.

>- DAVID was developped by the [CREAM Neuroscience Lab](http://cream.ircam.fr) at IRCAM with funding from the European Research Council (CREAM 335536, PI: [Jean-Julien Aucouturier](https://www.ircam.fr/person/jean-julien-aucouturier/)) and in collaboration with [Petter Johansson](https://portal.research.lu.se/portal/en/persons/petter-johansson(882697c1-3f92-44e1-8fb4-a61865c356c1).html)
and [Lars Hall](https://portal.research.lu.se/portal/en/persons/lars-hall(9d1bd794-a5bc-4e61-b90b-bd208667ec26).html) ([Lund University] (http://www.lunduniversity.lu.se), Sweden), Rodrigo Segnini (Siemens, Japan), [Katsumi Watanabe](https://waseda.pure.elsevier.com/en/persons/katsumi-watanabe) ([Waseda University](https://waseda.pure.elsevier.com/en/), Japan), and [Daniel Richardson](https://www.ucl.ac.uk/pals/research/experimental-psychology/person/daniel-richardson/) ([University College](university college london admission), London UK). DAVID was so named after Talking Heads’ frontman David Byrne, whom we were priviledged to count as our early users in March’15.
> <br><br></br>
>- Associated Publications (open-access): 
> [Rachman L.](https://www.ircam.fr/person/laura-rachman/), [Liuni M.](https://www.ircam.fr/person/marco-liuni/), [Arias P.] (https://www.ircam.fr/person/pablo-arias-sarah/), [Lind A.](https://portal.research.lu.se/portal/en/persons/andreas-lind(29a9f95e-91fb-4b96-9dd7-b0a0470dc46a).html), [Johansson P.](https://portal.research.lu.se/portal/en/persons/petter-johansson(882697c1-3f92-44e1-8fb4-a61865c356c1).html), [Hall L.](https://portal.research.lu.se/portal/en/persons/lars-hall(9d1bd794-a5bc-4e61-b90b-bd208667ec26).html), [Richardson D.](https://www.ucl.ac.uk/pals/research/experimental-psychology/person/daniel-richardson/), [Watanabe K.](https://waseda.pure.elsevier.com/en/persons/katsumi-watanabe), [Dubal S.](https://www.researchgate.net/scientific-contributions/2125674064_Stephanie_Dubal) and [Aucouturier J.J.](https://www.ircam.fr/person/jean-julien-aucouturier) (2017) DAVID: An open-source platform for real-time transformation of infra-segmental emotional cues in running speech. Behaviour Research Methods. doi: 10.3758/s13428-017-0873-y. 
> [DAVID: An open-source platform for real-time transformation of infra-segmental emotional cues in running speech]( https://link.springer.com/article/10.3758/s13428-017-0873-y)
> <br><br></br>
>- [Aucouturier J.J.](https://www.ircam.fr/person/jean-julien-aucouturier), [Johansson P.](https://portal.research.lu.se/portal/en/persons/petter-johansson(882697c1-3f92-44e1-8fb4-a61865c356c1).html), [Hall L.](https://portal.research.lu.se/portal/en/persons/lars-hall(9d1bd794-a5bc-4e61-b90b-bd208667ec26).html), [Segnini R.](https://www.linkedin.com/in/rodrigo-segnini-6221b6/?originalSubdomain=de), [Mercadié L.](https://www.linkedin.com/in/lolita-mercadié-6b909482/?originalSubdomain=fr) & [Watanabe K.](https://waseda.pure.elsevier.com/en/persons/katsumi-watanabe) (2016) [Covert Digital Manipulation of Vocal Emotion Alter Speakers’ Emotional State in a Congruent Direction](https://www.pnas.org/content/early/2016/01/05/1506552113). Proceedings of the National Academy of Sciences, vol. 113 no. 4, doi: 10.1073/pnas.1506552113. 
